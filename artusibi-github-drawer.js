/* LICENSE START */
/*
 * ssoup
 * https://github.com/alebellu/ssoup
 *
 * Copyright 2012 Alessandro Bellucci
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://alebellu.github.com/licenses/GPL-LICENSE.txt
 * http://alebellu.github.com/licenses/MIT-LICENSE.txt
 */
/* LICENSE END */

/* HEADER START */
var env, requirejs, define;
if (typeof module !== 'undefined' && module.exports) {
    env = "node";
    requirejs = require('requirejs');
    define = requirejs.define;

    requirejs.config({
        baseUrl: __dirname,
        nodeRequire: require
    });

    // require all dependant libraries, so they will be available for the client to request.
    var pjson = require('./package.json');
    for (var library in pjson.dependencies) {
        require(library);
    }
} else {
    env = "browser";
    //requirejs = require;
}

({ define: env === "browser"
        ? define
        : function(A,F) { // nodejs.
            var path = require('path');
            var moduleName = path.basename(__filename, '.js');
            console.log("Loading module " + moduleName);
            module.exports = F.apply(null, A.map(require));
            global.registerUrlContext("/" + moduleName + ".js", {
                "static": __dirname,
                defaultPage: moduleName + ".js"
            });
            global.registerUrlContext("/" + moduleName, {
                "static": __dirname
            });
        }
}).
/* HEADER END */
define(['jquery', 'artusi-kitchen-tools'], function($, tools) {

    var drdf = tools.drdf;
    var drff = tools.drff;

    var github = function(context) {
        var drawer = this;
        drawer.context = context;
    };

    /**
     * @options:
     *   @drawerIRI the IRI of the drawer.
     */
    github.prototype.init = function() {
        var dr = $.Deferred();
        var drawer = this;
        var kitchen = drawer.context.kitchen;

        // retrieve github auth info
        var authInfo = kitchen.shelf.authInfo["http://auth.artusi.ssoup.org/github"];

        // copy github specific options into easy typing options.
        drawer.conf.user = authInfo.user;
        drawer.conf.repo = drawer.conf["agh:repo"];
        drawer.conf.tree = drawer.conf["agh:tree"];
        drawer.conf.path = drawer.conf["agh:path"];
        drawer.conf.implicitPath = drawer.conf["agh:implicitPath"];

        /**
         * Registry indexes.
         */
        drawer.indexes = {};

        /**
         * Code of the recipes.
         */
        drawer.recipesCode = {};

        drawer.recipeBookCache = {};

        require(["jquery-github"], function(github) {
            drawer.gh = new github({
                useTreeCache: true
            });

            dr.resolve();
        });

        return dr.promise();
    };

    github.prototype.getIcon32Url = function() {
        return "http://github.drawer.artusi.ssoup.org/1.0/images/github_white_black_cat_32.png";
    };

    github.prototype.getServiceRecipes = function(options) {
        var drawer = this;
        var kitchen = drawer.context.kitchen;
        var dr = $.Deferred();

        if (kitchen.shelf.authorized('github')) {
            dr.resolve([{
                impl: {
                    "@type": "sr:recipeImpl",
                    "sr:recipeName": "buildIndexes",
                    "rdfs:label": "Build indexes"
                },
                interface: {
                    "@type": "sr:Recipe",
                    "sr:hasCategory": "sr:ServiceRecipe",
                    "rdfs:comment": "Rebuild indexes for Artusi GitHub Drawer",
                }
            }]);
        }
        else {
            dr.resolve([]);
        }

        return dr.promise();
    };

    github.prototype.ssoupRecipe = function(recipeIRI, recipeCode) {
        this.recipesCode[recipeIRI] = recipeCode;
    };

    /**
     * Artusi implementation of SSOUP [drawer.knows](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.knows)
     *
     * @options:
     * 	@resourceIRI the IRI of the resource to check the existence
     * @return true if the registry contains the resource with the given IRI as a subject of at least one triple, false otherwise.
     */
    github.prototype.knows = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        var resourceIRI = drawer.getRelativeResourceIRI(options.resourceIRI);
        if (!resourceIRI) {
            dr.resolve(false);
            return dr.promise();
        }

        var path = resourceIRI;
        // extract blob name from path
        var indexOfSlash = path.lastIndexOf('/');
        var blobName;
        if (indexOfSlash > 0) {
            blobName = path.substring(indexOfSlash + 1);
            path = path.substring(0, indexOfSlash);
        }
        else {
            blobName = path;
            path = null;
        }

        if (drawer.conf.implicitPath) {
            path = drawer.conf.implicitPath + "/" + path;
        }
        path = path.replace('//', '/'); // eliminate possible double /

        drawer.gh.tree({
            user: drawer.conf.user,
            repo: drawer.conf.repo,
            tree: drawer.conf.tree,
            path: drawer.conf.path + '/' + path
        }).done(function(tree) {
            if (tree) {
                // look for the blob sha
                var res = false;
                for (var i = 0; i < tree.tree.length; i++) {
                    if (tree.tree[i].type == 'blob' && tree.tree[i].path == blobName) {
                        res = true;
                        break;
                    }
                }
                dr.resolve(res);
            }
            else {
                dr.resolve(false);
            }
        }).fail(drdf(dr));

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.get](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.get)
     *
     * @options:
     *   @resourceIRI the IRI of the resource to retrieve
     *   @mode api/web the download mode: via api or via http get to a gh-pages web page, defaults to api
     *   @sha the blob sha if known. Alternative to resourceIRI
     * @return the resource.
     */
    github.prototype.getResource = function(options) {
        var dr = $.Deferred();
        var drawer = this;
        var kitchen = drawer.context.kitchen;

        var path;
        if (options.resourceIRI) {
            // resourceIRI can be either absolute or relative to the drawer path
            var resourceIRI = drawer.getRelativeResourceIRI(options.resourceIRI);
            if (!resourceIRI) {
                dr.resolve(undefined);
                return dr.promise();
            }
            path = drawer.conf.path + '/' + resourceIRI;
            if (drawer.conf.implicitPath) {
                path = drawer.conf.implicitPath + "/" + path;
            }
            path = path.replace('//', '/'); // eliminate possible double /
        }

        var mode = options.mode;
        if (!mode) {
            mode = "api";
        }

        if (mode == "api") {
            // api mode: this mode can incur in the GitHub API rate limit
            drawer.gh.blob({
                user: drawer.conf.user,
                repo: drawer.conf.repo,
                tree: drawer.conf.tree,
                path: path,
                sha: options.sha
            }).done(function(blob) {
                var content = window.atob(blob.content.replace(/\s/g, ''));
                dr.resolve(content);
            }).fail(function(error) {
                if (error && error.code == 'ERR_BLOB_001') {
                    // resource not found: do not fail
                    dr.resolve(undefined);
                }
                else {
                    dr.reject(error);
                }
            });
        }
        else if (mode == "web") {
            // web mode
            var url = "http://" + drawer.conf.user + ".github.com/" + drawer.conf.repo + "/" + path;
            //if ( path.match( /.html$/ )

            /*var frame = $( '<iframe src="' + url + '" />' ).css( "display", "none" );
	frame.load( function( event ) {
		var content = frame.find( 'body' )[0];
		alert( content );
		frame.remove();
	} );
	frame.appendTo( 'body' );*/

            kitchen.tools.getScripts([url]).done(function() {
                var content = latestSSOUPIngredient;
                dr.resolve(content);
            }).fail(function() {
                dr.reject("An error occurred while retrieving resource");
            });
        }

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.put](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.put)
     *
     * @options:
     *    @resourceIRI the IRI of the resource to save
     * 	@data the data to save to the register
     *	@message the commit message
     */
    github.prototype.put = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        // resourceIRI can be either absolute or relative to the drawer path
        var resourceIRI = drawer.getRelativeResourceIRI(options.resourceIRI);
        var path = drawer.conf.path + '/' + resourceIRI;
        if (drawer.conf.implicitPath) {
            path = drawer.conf.implicitPath + "/" + path;
        }
        path = path.replace('//', '/'); // eliminate possible double /

        var data = options.data;
        if (data && typeof data !== "string") {
            data = $.toJSON(data);
        }

        drawer.gh.commit({
            user: drawer.conf.user,
            repo: drawer.conf.repo,
            commit_ref: 'heads/' + drawer.conf.tree,
            tree: drawer.conf.tree,
            new_tree: [{
                path: path,
                content: data
            }],
            message: options.message
        }).done(function() {
            dr.resolve();
        }).fail(drdf(dr));

        return dr.promise();
    };

    /**
     * Checks whether the drawer knows about the pan.
     *
     * @options
     *   @panIRI the IRI of the pan
     */
    github.prototype.knowsPan = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        drawer.getPans().done(function(pans) {
            if (pans[options.panIRI]) {
                dr.resolve(true);
            }
            else {
                dr.resolve(false);
            }
        }).fail(drdf(dr));

        return dr.promise();
    };

    /**
     * Retrieve the pan with the given IRI.
     *
     * @options
     *   @panIRI the IRI of the pan to retrieve.
     */
    github.prototype.getPan = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        drawer.getPans().done(function(pans) {
            drawer.getResource({
                resourceIRI: pans[options.panIRI]["sc:panImpl"]
            }).done(drdf(dr)).fail(drff(dr));
        }).fail(drdf(dr));

        return dr.promise();
    };

    /**
     * Retrieves the list of pans in the drawer.
     *
     * @options
     */
    github.prototype.getPans = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        if (drawer.pans) {
            dr.resolve(drawer.pans);
        }
        else {
            drawer.getResource({
                resourceIRI: ".pans.jsonld"
            }).done(function(pans) {
                drawer.pans = pans;
                dr.resolve(drawer.pans);
            }).fail(drdf(dr));
        }

        return dr.promise();
    };

    /**
     * Retrieves info about the indexes in the drawer.
     *
     * @options
     */
    github.prototype.getIndexes = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        if (drawer.indexes && drawer.indexes.lastUpdate) {
            dr.resolve(drawer.indexes);
        }
        else {
            drawer.getResource({
                resourceIRI: ".indexes.jsonld"
            }).done(function(indexes) {
                drawer.indexes = indexes;
                if (drawer.indexes) {
                    drawer.indexes.lastUpdate = new Date();
                }
                dr.resolve(drawer.indexes);
            }).fail(drff(dr));
        }

        return dr.promise();
    };

    /**
     * Build all the indexes by scanning all the resources in the drawer.
     *
     * @options
     */
    github.prototype.buildIndexes = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        drawer.getIndexes().done(function(indexes) {
            if (indexes && indexes["aidxns:indexes"]) {
                var drs = [];
                $.each(indexes["aidxns:indexes"], function(i, index) {
                    var drl = drawer.buildIndex({
                        indexInfo: index
                    });
                    drs.push(drl);
                });
                $.when.apply($, drs).done(function() {
                    dr.resolve();
                });
            }
            else {
                dr.resolve();
            }
        });

        return dr.promise();
    }

    /**
     * Build an index by scanning all the resources in the drawer.
     *
     * @options
     *	@indexInfo the info of the index to rebuild.
     */
    github.prototype.buildIndex = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        var path = drawer.conf.path;
        if (drawer.conf.implicitPath) {
            path = drawer.conf.implicitPath + "/" + path;
        }
        path = path.replace('//', '/'); // eliminate possible double /

        drawer.gh.tree({
            user: drawer.conf.user,
            repo: drawer.conf.repo,
            tree: drawer.conf.tree,
            path: path
        }).done(function(root) {
            if (root) {
                drawer.gh.treeRecursive({
                    user: drawer.conf.user,
                    repo: drawer.conf.repo,
                    tree: root.sha
                }).done(function(tree) {
                    var index = {};
                    drawer.buildIndexForTree({
                        indexInfo: options.indexInfo,
                        index: index,
                        tree: tree
                    }).done(function() {
                        // write the index file
                        drawer.put({
                            resourceIRI: ".idx/" + options.indexInfo["aidx:indexFile"],
                            data: index,
                            message: "Index update"
                        }).done(drdf(dr)).fail(drff(dr));
                    }).fail(drff(dr));
                });
            }
            else {
                // TODO: raise an error here
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    /**
     * Update an index by rescanning all the resources in the specified tree.
     * Blob fetching can be parallelized.
     *
     * @options
     *	@indexInfo the info structure about the index to build
     *	@index the index to update.
     *	@tree the tree to scan.
     * @return an indexing structure for the tree
     */
    github.prototype.buildIndexForTree = function(options) {
        var deferreds = [];
        var drawer = this;

        for (var i = 0; i < options.tree.tree.length; i++) {
            var entry = options.tree.tree[i];
            if (entry.type == 'blob') {
                if (entry.path.indexOf(".") == 0) {
                    // skip "system" files starting with a dot.
                    continue;
                }
                if (entry.path.indexOf(".jsonld") <= 0) {
                    // for now only jsonld files are supported
                    continue;
                }
                var dr = $.Deferred();

                (function(resourceIRI, deferred) {
                    // put for loop dr variable in scope for inner closure
                    drawer.getResource({
                        sha: entry.sha
                    }).done(function(resource) {
                        if (options.indexInfo.filters) {
                            for (var j in options.indexInfo.filters) {
                                var filter = options.indexInfo.filters[j];
                                if (resource[filter["aidx:filterProperty"]] != filter["aidx:filterValue"]) {
                                    // skip this resource
                                    deferred.resolve();
                                    return;
                                }
                            }
                        }

                        var indexKey = '';
                        for (var j in options.indexInfo["aidx:indexedProperties"]) {
                            var indexedProperty = options.indexInfo["aidx:indexedProperties"][j];
                            if (resource[indexedProperty]) {
                                if (j > 0) {
                                    indexKey += ',';
                                }
                                if (resource[indexedProperty]) {
                                    indexKey += resource[indexedProperty].toString();
                                }
                            }
                        }
                        if (indexKey != '') {
                            if (!options.index[indexKey]) {
                                options.index[indexKey] = [];
                            }

                            var indexEntry = {};
                            // property names are kept short in index entries for index compactness
                            indexEntry.a = resourceIRI; // "address" of the resource
                            indexEntry.v = {}; // "value" of the resource
                            for (var j in options.indexInfo["aidx:trackedProperties"]) {
                                var trackedProperty = options.indexInfo["aidx:trackedProperties"][j];
                                indexEntry.v[trackedProperty] = resource[trackedProperty];
                            }
                            options.index[indexKey].push(indexEntry);
                        }
                        deferred.resolve();
                    }).fail(drff(deferred));
                })(entry.path, dr);

                deferreds.push(dr.promise());
            }
            /* Recursion is not needed here as GitHub api already flattens the hierarchy
		else if( entry.type == 'tree' ) {
        	var dr = driver.buildIndexForTree( {
            	indexInfo: options.indexInfo,
            	index: options.index,
            	tree: entry
            } );
            deferreds.push( dr.promise() );
        }*/
        }

        return $.when.apply($, deferreds).promise();
    };

    return github;
});

/**
 * This variable is used to retrieve an SSOUP ingredient via a script served via GitHub.
 */
var latestSSOUPIngredient;

function ssoupIngredient(ingredient) {
    latestSSOUPIngredient = ingredient;
}
